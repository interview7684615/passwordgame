import random
import string
import pytest
import re
from playwright.sync_api import Page, expect
from conftest import sum_digits_in_string, randomize_case, roman_lower_case, SPECIAL_CHARACTERS, MONTHS, ROMAN_NUMERALS, SPONSORS


def test_password_rules(page: Page, open_nealfun_page):
# 1st rule
# Trigger 1st rule by filling a random password with only 4 lowercase characters
    password = ''.join(random.choices(string.ascii_lowercase, k=4))
    page.locator(".ProseMirror").fill(password)
    expect(page.get_by_text("Your password must be at least 5 characters.")).to_be_visible()

# 2nd rule
# Pass 1st rule and trigger 2nd rule by adding 1 random lowercase character
    password = password + ''.join(random.choices(string.ascii_lowercase, k=1))
    page.locator(".ProseMirror").fill(password)
    expect(page.get_by_text("Your password must include a number.")).to_be_visible()

# 3rd rule
# Pass 2nd rule and trigger 3rd rule by adding 1 random digit
    password = password + ''.join(random.choices(string.digits, k=1))
    page.locator(".ProseMirror").fill(password)
    expect(page.get_by_text("Your password must include an uppercase letter.")).to_be_visible()

# 4th rule
# Pass 3rd rule and trigger 4th rule by adding 1 random uppercase letter
    password = password + ''.join(random.choices(string.ascii_uppercase, k=1))
    page.locator(".ProseMirror").fill(password)
    expect(page.get_by_text("Your password must include a special character.")).to_be_visible()

# 5th rule
# Pass 4th rule and trigger 5th rule by adding 1 random special character
    password = password + ''.join(random.choice(SPECIAL_CHARACTERS))
    page.locator(".ProseMirror").fill(password)
    expect(page.get_by_text("The digits in your password must add up to 25.")).to_be_visible()

# 6th rule
# Pass 5th rule and trigger 6th rule by adding as much digits as it is needed to achieve a sum of 25
    while sum_digits_in_string(password) != 25:
        if sum_digits_in_string(password) < 16:
            password = password + str(random.randint(0, 9))
        else:
            password = password + str(25 - sum_digits_in_string(password))
    page.locator(".ProseMirror").fill(password)
    expect(page.get_by_text("Your password must include a month of the year.")).to_be_visible()

# 7th rule
# Pass 6th rule and trigger 7th rule by adding a random month
    password = password + ''.join(random.choice(MONTHS))
    page.locator(".ProseMirror").fill(password)
    expect(page.get_by_text("Your password must include a roman numeral.")).to_be_visible()

# 8th rule
# Pass 7th rule and trigger 8th rule by adding random roman numeral
    password = password + ''.join(random.choice(ROMAN_NUMERALS))
    page.locator(".ProseMirror").fill(password)
    expect(page.get_by_text("Your password must include one of our sponsors:")).to_be_visible()

# 9th rule
# Pass 8th rule and trigger 9th rule by adding random sponsor
    password = password + ''.join(randomize_case(random.choice(SPONSORS)))
    page.locator(".ProseMirror").fill(password)
    expect(page.get_by_text("The roman numerals in your password should multiply to 35.")).to_be_visible()

# 10th rule
# Pass 9th rule and trigger 10th rule by making roman numerals lower case and add V and VII
    password = roman_lower_case(password) + ''.join('VIIV')
    page.locator(".ProseMirror").fill(password)
    expect(page.get_by_text("Your password must include this CAPTCHA:")).to_be_visible()
