import pytest
import random
import asyncio
from playwright.sync_api import Page, expect, sync_playwright, Playwright

SPECIAL_CHARACTERS = "!@#$%^&*()_+-=[]{}|;:,.<>?~"
MONTHS = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']
ROMAN_NUMERALS = ['I', 'V', 'X', 'L', 'C', 'D', 'M']
SPONSORS = ['pepsi', 'starbucks', 'shell']


@pytest.fixture
def open_nealfun_page(page: Page):
    page.goto("https://neal.fun/password-game/")
# Accept cookies
    try:
        page.get_by_label("Zgadzam się", exact=True).click()
    except:
        expect(page.get_by_text("Please choose a password")).to_be_visible()
        

def sum_digits_in_string(input_string):
    total = 0
    for char in input_string:
        if char.isdigit():
            total += int(char)
    return total

def randomize_case(input_string):
    randomized_string = ''
    for char in input_string:
# Randomly decide whether to convert the character to uppercase or lowercase
        if random.choice([True, False]):
            randomized_string += char.upper()
        else:
            randomized_string += char.lower()
    return randomized_string

def roman_lower_case(input_string):
    output_string = ''
    for char in input_string:
# Lower case roman letters
        if char in ROMAN_NUMERALS:
            output_string += char.lower()
        else:
            output_string += char
    return output_string
